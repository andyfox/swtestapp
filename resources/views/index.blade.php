@extends('layouts.app')

@section('content')
    <div class="container mt-5">
        <a href="/click?param1=11&param2=22" target="_blank">/click?param1=11&param2=22</a>
    </div>
    <div class="container mt-5">
        <h2 class="mb-4">Clicks</h2>
        <table class="table table-bordered yajra-datatable">
            <thead>
            <tr>
                <th>id</th>
                <th>user agent</th>
                <th>ip</th>
                <th>ref</th>
                <th>param1</th>
                <th>param2</th>
                <th>errors</th>
                <th>bad domain</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    <script type="text/javascript">
        $(function () {

            var table = $('.yajra-datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "/api/clicks",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'ua', name: 'ua'},
                    {data: 'ip', name: 'ip'},
                    {data: 'ref', name: 'ref'},
                    {data: 'param1', name: 'param1'},
                    {data: 'param2', name: 'param2'},
                    {data: 'error', name: 'error'},
                    {data: 'bad_domain', name: 'bad_domain'},
                ]
            });

        });
    </script>
@endsection





