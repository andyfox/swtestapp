@extends('layouts.app')

@section('content')
    <div class="container mt-5">
        <h2 class="mb-4">Bad Domains</h2>
        <table class="table table-bordered yajra-datatable">
            <thead>
            <tr>
                <th>id</th>
                <th>name</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <div class="col-sm-4 pt-5">
            <div class="card card-primary card-outline">
                <div class="card-body">
                    <div class="form-group">
                        <input type="text" class="form-control" id="name" placeholder="add bad domain">
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" id="addBadDomain" class="btn btn-primary">Add</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function () {

            var table = $('.yajra-datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "/api/bad-domains",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                ]
            });

            $(document).on("click", '#addBadDomain', function (e) {
                $.ajax({
                    type: "POST",
                    cache: false,
                    url: "/api/bad-domains",
                    data: {
                        name: $("#name").val()
                    }
                }).done(function (result) {
                    table.ajax.reload();
                })
            });

        });
    </script>
@endsection





