<script src="/js/app.js"></script>

    <div class="title m-b-md">
        Error {{$click_id}}
    </div>

    @if(session('error'))
        <div class="alert alert-danger"> {{ session('error') }} </div>
    @endif

    @if(env('REDIRECT_TO'))
        <div>You will be redirected to <span id="url">{{ env('REDIRECT_TO') }}</span> in <span id="time">5</span> seconds!</div>
    @endif

