<?php


namespace App\DataTables;

use App\Models\Click;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class ClicksDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->toJson();
    }

    /**
     * Get query source of dataTable.
     *
     * @param Click $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Click $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('clicks-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(0)
            ->buttons(
                Button::make('reload')
            )
            ->ajax([
                'url' => '/api/clicks',
                "type" => 'POST',
                "headers" => [
                    'X-CSRF-TOKEN'=> csrf_token()
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id'),
            Column::make('ua'),
            Column::make('ip'),
            Column::make('ref'),
            Column::make('param1'),
            Column::make('param2'),
            Column::make('errors'),
            Column::make('bad_domain'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Clicks_' . date('YmdHis');
    }
}
