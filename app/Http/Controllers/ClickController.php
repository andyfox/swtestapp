<?php


namespace App\Http\Controllers;

use App\DataTables\ClicksDataTable;
use App\Http\Services\ClickService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class ClickController extends Controller
{
    /**
     * @var ClickService
     */
    private $clickService;

    public function __construct(ClickService $clickService)
    {
        $this->clickService = $clickService;
    }

    public function index(ClicksDataTable $dataTable)
    {
        return $dataTable->render('index');
    }

    public function add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'param1' => 'required|max:255',
            'param2' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect('error')
                ->withErrors($validator)
                ->withInput();
        }

        $click = $this->clickService->add(
            $request->userAgent(),
            $request->ip(),
            $request->headers->get('referer'),
            $request->get('param1'),
            $request->get('param2')
        );

        if ($click->error) {
            return redirect()->route('error', [$click->id]);
        } else {
            return redirect()->route('success', [$click->id]);
        }
    }

    public function success($click_id)
    {
        return view('success', ['click_id' => $click_id]);
    }

    public function error($click_id = null)
    {
        return view('error', ['click_id' => $click_id]);
    }
}
