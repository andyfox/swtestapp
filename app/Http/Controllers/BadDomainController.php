<?php


namespace App\Http\Controllers;


use App\DataTables\BadDomainsDataTable;

class BadDomainController
{
    public function index(BadDomainsDataTable $dataTable)
    {
        return $dataTable->render('bad-domains');
    }
}
