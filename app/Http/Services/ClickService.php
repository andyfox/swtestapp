<?php


namespace App\Http\Services;


use App\Helpers\Hasher;
use App\Models\Click;

class ClickService
{
    private $badDomainService;

    public function __construct(BadDomainService $badDomainService)
    {
        $this->badDomainService = $badDomainService;
    }

    /**
     * @param string $userAgent
     * @param string $ip
     * @param string $ref
     * @param string $param1
     * @param string $param2
     * @return Click
     */
    public function add($userAgent, $ip, $ref, $param1, $param2)
    {
        $id = Hasher::make([$userAgent, $ip, $ref, $param1, $param2]);

        $click = Click::find($id);

        if ($click) {
            $badDomain = false;

            if ($this->badDomainService->isBadDomain($ref)) {
                $badDomain = true;
            }

            $this->error($click, $badDomain);
        } else {
            $click = new Click();
            $click->id = $id;
            $click->ua = $userAgent;
            $click->ip = $ip;
            $click->ref = parse_url($ref, PHP_URL_HOST);
            $click->param1 = $param1;
            $click->param2 = $param2;
            $click->save();
        }

        return Click::find($id);
    }

    /**
     * @param Click $click
     * @param bool $badDomain
     */
    public function error(Click $click, bool $badDomain = false)
    {
        $click->error++;
        $click->bad_domain = $badDomain;
        $click->save();
    }
}
