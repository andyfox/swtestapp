<?php


namespace App\Http\Services;


use App\Models\BadDomain;

class BadDomainService
{
    public function all()
    {
        return BadDomain::query()->get('name');
    }

    /**
     * @param string $name
     * @return BadDomain
     */
    public function add(string $name)
    {
        $badDomain = new BadDomain();
        $badDomain->name = $name;
        $badDomain->save();

        return $badDomain;
    }

    /**
     * @param string $domain
     * @return bool
     */
    public function isBadDomain(string $domain)
    {
        return BadDomain::query()->where('name', $domain)->count();
    }
}
