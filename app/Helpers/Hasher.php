<?php


namespace App\Helpers;


class Hasher
{
    public static function make(array $array): string
    {
        return hash('md5', implode('', $array));
    }
}
