<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Click
 * @package App\Models
 *
 * @property string $id
 * @property string $ua
 * @property string $ip
 * @property string $ref
 * @property string $param1
 * @property string $param2
 * @property integer $error
 * @property boolean $bad_domain
 */

class Click extends Model
{
    use HasFactory;

    protected $keyType = 'string';

    protected $fillable = [
        'ua',
        'ip',
        'ref',
        'param1',
        'param2',
        'error',
        'bad_domain',
    ];
}
