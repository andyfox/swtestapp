<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClicksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clicks', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string("ua")->nullable();
            $table->ipAddress("ip");
            $table->string("ref")->nullable();
            $table->string("param1")->nullable();
            $table->string("param2")->nullable();
            $table->unsignedInteger("error")->default(0);
            $table->boolean("bad_domain")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clicks');
    }
}
