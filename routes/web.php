<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/click', [\App\Http\Controllers\ClickController::class, 'add']);
Route::get('/', [\App\Http\Controllers\ClickController::class, 'index'])->name('index');
Route::get('/success/{click_id}', [\App\Http\Controllers\ClickController::class, 'success'])->name('success');
Route::get('/error/{click_id?}', [\App\Http\Controllers\ClickController::class, 'error'])->name('error');

Route::get('/bad-domains', [App\Http\Controllers\BadDomainController::class, 'index'])->name('bad-domains');


Auth::routes();


